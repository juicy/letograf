$(document).ready(function(){
  fix_height('#wrapper');
});

function fix_height (box) {
  $(box).css('height', parseInt($('html').height()) - 99 - 50);
  $(box).css('overflow', 'scroll');
}