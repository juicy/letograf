$(document).ready(function(){
	selectOpen('.select div.value');
	selectClose('.select a.value');
	selectOpen('.filter div.block');
	filterClose('.filter .over a.close');
});

function filterClose (a) {
	$(a).click(function(){
		$(this).parent().parent().css('display', 'none');
		return false;
	});
}

function selectOpen (div) {
	$(div).click(function(){
		var 	over = $(this).next();
		if ($(over).css('display') == 'block' ) {
			$(over).css('display', 'none');	
		} else {
			$(over).css('display', 'block');	
		}
	});   
}   

function selectClose (div) {
	$(div).click(function(){
		var 	over = $(this).parent(), 
					valu = $(over).prev();
		$(valu).find('span').html($(this).html());
		$(over).css('display', 'none');
	});
}