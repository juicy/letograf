var isc = {
	kzcon: {
		Map: function(object) {
			var region = eval(object.map);
			
			var regions = object.regions;
			var points = object.points;
			
			var onRegionSelect = object.onRegionSelect;
			var renderTo = object.renderTo;
			
			var width = object.width;
			var height = object.height;
			
			var kh = (width  == null) ? height/region.map.height : width/region.map.width;
			var kw = (height == null) ? width/region.map.width : height/region.map.height;
			
			if (width == null) {
				width = region.map.width * kw;
			}
			if (height == null) {
				height = region.map.height * kh;
			}              
			
			jQuery('#map').css({
				'width': width,
				'height': height
			});

			var Rap = Raphael(renderTo, width, height);
			
			for (var item in regions) {
				var obj = region.regions[regions[item].code];
				
				var p = Rap.path(obj.path);
					p.attr({cursor: "pointer", fill: regions[item].color, stroke: strokeColor, "stroke-width": strokeWidth, "stroke-linejoin": "round"});
					p.scale(kh, kw, 0, 0);

				var t = Rap.text(obj.cx, obj.cy, regions[item].short);
					t.scale(kh, kw, 0, 0);
					t.attr({cursor: "pointer"});
				
		        (function (sp, st) {
					sp[0].long = st[0].long = regions[item].long;
					sp[0].code = st[0].code = regions[item].code;
					sp[0].color = st[0].color = regions[item].color;
					
		            sp[0].onmouseover = st[0].onmouseover = function () {
		                sp.animate({fill: selectFillColor}, time);
		                Rap.safari();
		            };
		            sp[0].onmouseout = st[0].onmouseout = function () {
		                sp.animate({fill: this.color}, time);
		                Rap.safari();
		            };
					sp[0].onclick = st[0].onclick = function () {
		                onRegionSelect(this.code+": "+this.long);
						Rap.safari();
		            };
		        })(p, t);
		    }
			
			for (var item in points) {
				var obj = region.points[points[item].code];
				
				var p = Rap.circle(obj.px, obj.py, obj.r);
					p.attr({"fill": points[item].color, "stroke": pointStrokeColor, "stroke-width": pointStrokeWidth, cursor: "pointer"});
					p.scale(kh, kw, 0, 0);
				
				var l = Rap.path(obj.path);
					l.scale(kh, kw, 0, 0);
				
				var t = Rap.text(obj.tx, obj.ty, points[item].short);
					t.attr({"fill": normalTitleFillColor, cursor: "pointer"});
					t.scale(kh, kw, 0, 0);

		        (function (sp, st) {
					sp[0].long = st[0].long = points[item].long;
					sp[0].code = st[0].code = points[item].code;
					sp[0].color = st[0].color = points[item].color;
					
		            sp[0].onmouseover = st[0].onmouseover = function () {
		                sp.animate({fill: selectPointFillColor}, time);
		                st.animate({fill: selectTitleFillColor}, time);
		                Rap.safari();
		            };
		            sp[0].onmouseout = st[0].onmouseout = function () {
		                sp.animate({fill: this.color}, time);
		                st.animate({fill: normalTitleFillColor}, time);
		                Rap.safari();
		            };
					sp[0].onclick = st[0].onclick = function () {
		                onRegionSelect(this.code+": "+this.long);
						Rap.safari();
		            };
		        })(p, t);
		    }
		}
	}
}